--------------------------------------------------------------------------------
-- Company: SAMEER, Mumbai
-- Engineer: Salil Tembe
--
-- Create Date:   12:04:10 01/05/2015
-- Design Name:   DCM DRP
-- Module Name:   F:/vhdl/spi_slave_test/tb_toggler.vhd
-- Project Name:  DCM DRP
-- Target Device: Xilinx Spartan 6 FT(G) 256
-- Tool versions:  ISE 14.7
-- Description:   This logic is used for testing the DCM DRP component. 
-- 
-- VHDL Test Bench Created by ISE for module: spi_toggler
-- 
-- Dependencies:
-- 
-- Revision: 0
-- Revision 1 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench instantiates the SPI Master to generate simulated input
-- of D-1 and M-1.
-- The SPI Master sends 16 bits of data, of which the first byte consists of
-- D-1 (MSB First) and M-1 (MSB First). 
-- This 16 bits of data is clocked into the RX data register of the SPI Slave
-- component. 
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.std_logic_arith.all ; 
USE IEEE.numeric_std.ALL;
use ieee.std_logic_unsigned.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY tb_toggler IS
generic( data_length : integer := 16 );
END tb_toggler;
 
ARCHITECTURE behavior OF tb_toggler IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT spi_toggler
    PORT(
         Clk50 : IN  std_logic;
         SCK : IN  std_logic;
         SS : IN  std_logic;
         MOSI : IN  std_logic;
         MISO : OUT  std_logic;
	    CLK100 : OUT std_logic;
         pin : OUT  std_logic;
	    TX_Data_s : out std_logic_vector(data_length-1 downto 0);
	    RX_Data_s : out std_logic_vector(data_length-1 downto 0)
        );
    END COMPONENT;
    
     -- SPI Master for simulation		
    COMPONENT SPI_Core
    Port ( TX_Data  : in  STD_LOGIC_VECTOR (15 downto 0); -- Sendedaten
           RX_Data  : out STD_LOGIC_VECTOR (15 downto 0); -- Empfangsdaten
           MOSI     : out STD_LOGIC;
           MISO     : in  STD_LOGIC;
           SCLK     : out STD_LOGIC;
           SS       : out STD_LOGIC;
           TX_Start : in  STD_LOGIC;
           TX_Done  : out STD_LOGIC;
           clk      : in  STD_LOGIC
         );
  END COMPONENT;	
   --Inputs
   signal Clk50 : std_logic := '0';
   signal SCK : std_logic := '0';
   signal SS : std_logic := '0';
   signal MOSI : std_logic := '0';
   signal mSCK : std_logic := '0';
   signal mSS : std_logic := '0';
   signal mMOSI : std_logic := '0';

 	--Outputs
   signal MISO : std_logic;
   signal mMISO : std_logic;
   signal pin : std_logic;
   
   --SPI MASTER
   signal TX_Data : std_logic_vector(15 downto 0);
   signal TX_Start : std_logic;
   signal RX_Data : std_logic_vector(15 downto 0);
   signal TX_Done : std_logic;
   
   signal TX_reg : std_logic_vector(data_length-1 downto 0);
   signal RX_reg : std_logic_vector(data_length-1 downto 0);
   signal CLK : std_logic;
   
   signal random : std_logic_vector(15 downto 0) := x"5401";
   -- Clock period definitions
   constant Clk50_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: spi_toggler PORT MAP (
          Clk50 => Clk50,
          SCK => SCK,
          SS => SS,
          MOSI => MOSI,
          MISO => MISO,
		  CLK100 => CLK,
          pin => pin,
		  TX_Data_s => TX_reg,
		  RX_Data_s => RX_reg
        );


	  master: SPI_Core PORT MAP(
	    TX_Data => TX_Data,
	    RX_Data => RX_Data,
	    MOSI => mMOSI,
	    MISO => mMISO,
	    SCLK => SCK,
	    SS => mSS,
	    TX_Start => TX_Start,
	    TX_Done => TX_Done,
	    clk => clk
	);
	
	MOSI <= mMOSI;
	MISO <= mMISO;
	SS   <= mSS;

   -- Clock process definitions
   Clk50_process :process
   begin
		Clk50 <= '0';
		wait for Clk50_period/2;
		Clk50 <= '1';
		wait for Clk50_period/2;
   end process;
 
 

   -- Stimulus process
   stim_proc: process
   begin		
        -- insert stimulus here 
	-- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for CLK50_period*10;

      -- insert stimulus here 
	 TX_Data <= random + 3;
	 random <= random + 3;
	 wait for 10 us;
	 TX_Start <= '1';

	 wait until TX_Done = '1';
	 TX_Start <= '0';
   end process;
	--MOSI_slave <= MISO_slave;

END;
