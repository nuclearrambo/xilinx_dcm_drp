--------------------------------------------------------------------------------
-- Company: SAMEER, Mumbai
-- Engineer: Salil Tembe
--
-- Create Date:   12:04:10 01/05/2015
-- Design Name:   DCM DRP
-- Module Name:   F:/vhdl/spi_slave_test/tb_toggler.vhd
-- Project Name:  DCM DRP
-- Target Device: Xilinx Spartan 6 FT(G) 256
-- Tool versions:  ISE 14.7
-- Description: This code takes the 16 bit command from the SPI Slave component
--				and programs the DCM DRP in order to change the clock frequency.
--
-- Dependencies: SPI_Slave.vhd
--
-- Revision: 1
-- Revision 1 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity spi_toggler is
	generic ( data_length : integer := 16;
	config_packet_length : integer := 35
	);

	port (
			Clk50     : in std_logic;
			SCK		: in std_logic;
			SS		: in std_logic;
			MOSI		: in std_logic;
			MISO		: out std_logic;
			CLK100	: out std_logic; 
			pin  	: out std_logic := '0';
			TX_Data_s : out std_logic_vector(data_length-1 downto 0);
			RX_Data_s : out std_logic_vector(data_length-1 downto 0);
			--DRP debug
			prog_en, prog_clk, prog_data : out std_logic
		);
end spi_toggler;

architecture Behavioral of spi_toggler is
	attribute keep 	 : string;
	--Inputs
	signal SCK_slave : std_logic := '0';
	signal MOSI_slave : std_logic := '0';
	signal SS_slave : std_logic := '1';
	signal RX_Data_slave : std_logic_vector(data_length-1 downto 0) := x"0000";
	signal TX_Data_slave : std_logic_vector(data_length-1 downto 0) := x"0000";
	signal SS_prev : std_logic := '0';
	--Outputs
	signal MISO_slave : std_logic;
	signal CLK		 : std_logic;
	signal toggle_bit : std_logic := '0';
	signal RX_Data_prev : std_logic_vector(data_length-1 downto 0) := x"0000";
	--EIKA
	signal config_packet : std_logic_vector(config_packet_length-1 downto 0);
	attribute keep of config_packet : signal is "TRUE";
  	signal progen_packet : std_logic_vector(config_packet_length-1 downto 0) :=  b"01111111111000001111111111000001000";
  	--attribute keep of progen_packet : signal is "TRUE";
  	signal LoadD		 : std_logic_vector(9 downto 0) := b"1000000000";
 	signal LoadM		 : std_logic_vector(9 downto 0) := b"1100000000";
    signal conf_trig : std_logic := '0';
    signal load_done   : std_logic := '0';
    signal DO_count	 : integer range 0 to 255;
    signal delay_counter : integer range 0 to config_packet_length;	
--  signal prog_en     : std_logic := '0';
--  signal prog_clk    : std_logic;
--  signal prog_done	 : std_logic;
    signal dcm_reset   : std_logic := '0';
  
   --DRP
   signal PROGCLK, PROGEN, PROGDATA, PROGDONE : std_logic;
   signal PROGCLK_raw : std_logic;
	COMPONENT SPI_Slave
		PORT(
			CLK  : IN std_logic := '0';
			SCK_slave : IN  std_logic;
			MOSI_slave : IN  std_logic;
			MISO_slave : OUT  std_logic;
			SS_slave : IN  std_logic;
			TX_Data_slave : in std_logic_vector(data_length-1 downto 0);
			RX_Data_slave : out std_logic_vector(data_length-1 downto 0)
		);
		END COMPONENT;

	component dcm
		port
			(-- Clock in ports
				Clk50           : in     std_logic;
				-- Clock out ports
				CLK          : out    std_logic;
				-- Dynamic reconfiguration ports
				PROGCLK           : in     std_logic;
				PROGEN            : in     std_logic;
				PROGDATA          : in     std_logic;
				PROGDONE          : out    std_logic
			);
		end component;
    

begin

   
   -- Instantiate the Unit Under Test (UUT)
   uut: SPI_Slave PORT MAP (
		CLK => CLK,
          SCK_slave => SCK,
          MOSI_slave => MOSI,
          MISO_slave => MISO,
          SS_slave => SS,
		TX_Data_slave => TX_Data_slave,
		RX_Data_slave => RX_Data_slave
        );

	DCM_DRP : dcm
	port map
	(-- Clock in ports
		Clk50 => Clk50,
	 -- Clock out ports
		CLK => CLK,
	 -- Dynamic reconfiguration ports
	    PROGCLK => PROGCLK,
	    PROGEN => PROGEN,
	    PROGDATA => PROGDATA,
	    PROGDONE => PROGDONE);
	CLK100 <= CLK;

	
	pin <= toggle_bit;
	
	RX_Data_s <= RX_Data_slave;
	TX_Data_s <= TX_Data_slave;
	
process(SS, SS_prev, CLK, load_done, conf_trig)
begin
	
	if rising_edge(CLK) then
		if SS /= SS_prev  then
			SS_prev <= SS;
			if SS_prev = '0' then
				toggle_bit <= not toggle_bit;
				DO_count <= 0;
				progen_packet <= b"11111111110000011111111110000010000";
				config_packet <= b"10" & RX_Data_slave(15 downto 8) & b"00000" & b"11" & RX_Data_slave(7 downto 0) & b"0000000000";
				load_done <= '1';
			end if;
		end if;
		if load_done = '1' then
			if DO_count =  34 then
				DO_count <= 0;
				load_done <= '0';
			else --Increment the bit counter and shift out PROGEN and PROGDATA
				for i in 0 to config_packet_length-2 loop
					config_packet(i+1) <= config_packet(i);
					progen_packet(i+1) <= progen_packet(i);
				end loop;
				DO_count <= DO_count + 1;
			end if;
			PROGDATA <= config_packet(34);
			PROGEN <= progen_packet(34);
		end if;
	end if;

end process;
	PROGCLK_raw <= CLK;
	PROGCLK <= CLK;

	--DRP debug
	prog_en <= PROGEN;
	prog_clk <= CLK;
	prog_data <= PROGDATA;
end Behavioral;

